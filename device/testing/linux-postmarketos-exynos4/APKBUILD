# Maintainer: Thiago Foganholi <thiagaoplusplus@outlook.com>
# Co-Maintainer: Newbyte <newbyte@disroot.org>
# Kernel config based on: arch/arm/configs/exynos_defconfig

pkgname=linux-postmarketos-exynos4
pkgver=5.16.2
pkgrel=0
pkgdesc="Mainline kernel fork for Samsung Exynos4 devices"
arch="armv7"
_carch="arm"
_flavor="${pkgname#linux-}"
url="https://kernel.org"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-anbox
	pmb:kconfigcheck-nftables
	"
makedepends="
	bison
	busybox-static-armv7
	findutils
	flex
	gmp-dev
	mpc1-dev
	mpfr-dev
	openssl-dev
	perl
	postmarketos-installkernel
	xz
"

# Source
_config="config-$_flavor.$arch"
case $pkgver in
	*.*.*)	_kernver=${pkgver%.0};;
	*.*)	_kernver=$pkgver;;
esac
source="
	https://cdn.kernel.org/pub/linux/kernel/v${_kernver%%.*}.x/linux-$_kernver.tar.xz
	$_config
	0001-ARM-decompressor-Flush-tlb-before-swiching-domain-0-.patch
	0002-ARM-dts-exynos-Add-reboot-modes-to-midas.patch
	0003-mmc-core-Workaround-VTU00M-0xf1-FTL-metadata-corrupt.patch
	0004-drivers-drm-Add-backlight-control-support-for-s6e8aa.patch
	0005-power_supply-max77693-Listen-for-cable-events-and-en.patch
	0006-mfd-max77693-Add-defines-for-charger-current-control.patch
	0007-power_supply-max77693-change-the-supply-type-to-POWE.patch
	0008-ARM-dts-exynos-i9100-Use-interrupt-for-BCM4330-ho.patch
	0009-ARM-dts-driver-exynos-n710x-add-panel.patch
	initramfs.list
	init
"
builddir="$srcdir/linux-${_kernver//_/-}"

prepare_isorec() {
	# https://wiki.postmarketos.org/wiki/Boot_process#isorec
	cp -v /usr/$(arch_to_hostspec $arch)/bin/busybox.static \
		"$builddir"/usr/
	cp -v "$srcdir"/init "$builddir"/usr/
	cp -v "$srcdir"/initramfs.list "$builddir"/usr/
}

prepare() {
	default_prepare
	prepare_isorec
	cp -v "$srcdir/$_config" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS"
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"
}

sha512sums="
197f387d5e3a5029e354aaf50dc4fffc20a894c2579132678396e69c567c4072ea365ade7484a0b964572829f4bbfe518f21df5704ae1685f5786fddbb321097  linux-5.16.2.tar.xz
b3aae956d4f21c3d3e366ed322d849d10116d5d425d3535b3d2ed8228459c461309fbe3c0561660116b6c283b497e99eeec0857998abc01e277d63b7232a2f23  config-postmarketos-exynos4.armv7
a033fa9afa05825d5c923f1d5a224e230a8b02e75b02a21e0a0077f1b76c01d663ba54204e66db1e85096d9e44cc29fee0acaf919d3187cb47dba9005f94d8be  0001-ARM-decompressor-Flush-tlb-before-swiching-domain-0-.patch
57590b105cb6d01e5f2d860a21c1417b197abbcaf8553bf57633b261ac1e161b0d424f8baeb7b14b3ed923ebac6f6e27401ef02c0b53a4b3e34600368fe85219  0002-ARM-dts-exynos-Add-reboot-modes-to-midas.patch
81430b484a0747717ed8dba45c04c40689d9546b7b9d3eb20f0520e78c94f1438afc1f21a0b06007f60ff718e924d1586acc98f4498671a1a7609ed6e6f2b3f0  0003-mmc-core-Workaround-VTU00M-0xf1-FTL-metadata-corrupt.patch
0e3aad5d8306c5a957cd8351078cedc583517c2821c0e82e57aeaaffdbf27db32214cdff4607134d5a554645186613540867c9f8c2a1456fa090cee7df2f5e3a  0004-drivers-drm-Add-backlight-control-support-for-s6e8aa.patch
65049c997705f3b3fe7feff089009b16f142d9138cd5bdd039535640dcabda8c5b3dac0238e0996e31bd909ff11e4cc82aab94dc5c518b641b71af4f7284cae6  0005-power_supply-max77693-Listen-for-cable-events-and-en.patch
7711e7b7fda4f089fa510ea38431fcf0cad80cbba381b3dddb57118e469b5eb868024cf92c9fa91f892ba2ea060009dbbc522e3bd873d9045338d148cfdd199b  0006-mfd-max77693-Add-defines-for-charger-current-control.patch
de87e6a6608165cdc35a03bd10dedef1c12a2a9e1f66f500879c05b287bae7181cc95021a3a2aad6abefd024f508ed61c19ee9341ad3cc24e260269b647e1010  0007-power_supply-max77693-change-the-supply-type-to-POWE.patch
4ad139898236dd7035ace862c4a9be2d92351977f2d0c38072434c82f2a7f89c224f9b366fcf44755849788f92ed8cdc019cf6c82453b85d5257bf6737a6bb68  0008-ARM-dts-exynos-i9100-Use-interrupt-for-BCM4330-ho.patch
054a03b327386b32b516cf7f9fdbf15adba9b3fb6d0d20473493f7e4538202496829443eb18f9a928b4959b1296ee98171d4a19d8fcf58439d4fdb122fbb97ac  0009-ARM-dts-driver-exynos-n710x-add-panel.patch
aaff0332b90e1f9f62de1128cace934717336e54ab09de46477369fa808302482d97334e43a85ee8597c1bcab64d3484750103559fea2ce8cd51776156bf7591  initramfs.list
09f1f214a24300696809727a7b04378887c06ca6f40803ca51a12bf2176a360b2eb8632139d6a0722094e05cb2038bdb04018a1e3d33fc2697674552ade03bee  init
"
