# Maintainer: Caleb Connolly <caleb@connolly.tech>
# Co-Maintainer: Joel Selvaraj <jo@jsfamily.in>
# Stable Linux kernel with patches for SDM845 devices
# Kernel config based on: arch/arm64/configs/defconfig and sdm845.config

_flavor="postmarketos-qcom-sdm845"
pkgname=linux-$_flavor
pkgver=5.16.5
pkgrel=0
pkgdesc="Mainline Kernel fork for SDM845 devices"
arch="aarch64"
_carch="arm64"
url="https://gitlab.com/sdm845-mainline/linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-nftables
	pmb:kconfigcheck-containers
	pmb:kconfigcheck-zram
	pmb:kconfigcheck-anbox
	pmb:kconfigcheck-iwd"
makedepends="bison findutils flex installkernel openssl-dev perl"

_repo="linux"
_config="config-$_flavor.$arch"
_tag="sdm845-5.16.5"

# Source
source="
	$_repo-$_tag.tar.gz::https://gitlab.com/sdm845-mainline/$_repo/-/archive/$_tag/$_repo-$_tag.tar.gz
	$_config
"
builddir="$srcdir/$_repo-$_tag"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$arch" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor"
}

package() {
	mkdir -p "$pkgdir"/boot

	install -Dm644 "$builddir/arch/$_carch/boot/Image.gz" \
		"$pkgdir/boot/vmlinuz"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot/ \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="
a3361b418e21351257eda435769130f50204f77aaab3a369e3f96a2b46fe8c0508d90b6abec2321db90580415a5b6901c61fe69603dc25cdc1a34f1aebdc1004  linux-sdm845-5.16.5.tar.gz
afc58f94ccc00c7bfad3dc56157830c5aed01282d6050b8bdb2cd2c932aafab7a4a1145977f06cd769a41f0baec160f71f15730b9838225f65a37de83b1dd8af  config-postmarketos-qcom-sdm845.aarch64
"
